﻿namespace CommentTranslater
{
    partial class FrmTranslater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dlgSelectFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.dlgSelectFile = new System.Windows.Forms.OpenFileDialog();
            this.lblSelectFolder = new System.Windows.Forms.Label();
            this.lblFile = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.btnSelectFolder = new System.Windows.Forms.Button();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.txtSheetName = new System.Windows.Forms.TextBox();
            this.lblSheetName = new System.Windows.Forms.Label();
            this.btnSaveComment = new System.Windows.Forms.Button();
            this.txtLinkSaveComment = new System.Windows.Forms.TextBox();
            this.lblLinkSaveComment = new System.Windows.Forms.Label();
            this.dlgSelectFolderSaveComment = new System.Windows.Forms.FolderBrowserDialog();
            this.btnGetComment = new System.Windows.Forms.Button();
            this.btnReadCommentData = new System.Windows.Forms.Button();
            this.btnTranslate = new System.Windows.Forms.Button();
            this.grdComment = new System.Windows.Forms.DataGridView();
            this.txtFileOutputEncoding = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdComment)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSelectFolder
            // 
            this.lblSelectFolder.AutoSize = true;
            this.lblSelectFolder.Location = new System.Drawing.Point(12, 13);
            this.lblSelectFolder.Name = "lblSelectFolder";
            this.lblSelectFolder.Size = new System.Drawing.Size(52, 17);
            this.lblSelectFolder.TabIndex = 0;
            this.lblSelectFolder.Text = "Folder:";
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.Location = new System.Drawing.Point(12, 47);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(34, 17);
            this.lblFile.TabIndex = 1;
            this.lblFile.Text = "File:";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(119, 10);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(453, 22);
            this.txtFolder.TabIndex = 2;
            this.txtFolder.TextChanged += new System.EventHandler(this.TxtFolder_TextChanged);
            // 
            // btnSelectFolder
            // 
            this.btnSelectFolder.Location = new System.Drawing.Point(578, 7);
            this.btnSelectFolder.Name = "btnSelectFolder";
            this.btnSelectFolder.Size = new System.Drawing.Size(75, 28);
            this.btnSelectFolder.TabIndex = 3;
            this.btnSelectFolder.Text = "Select";
            this.btnSelectFolder.UseVisualStyleBackColor = true;
            this.btnSelectFolder.Click += new System.EventHandler(this.BtnSelectFolder_Click);
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Location = new System.Drawing.Point(578, 41);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(75, 29);
            this.btnSelectFile.TabIndex = 4;
            this.btnSelectFile.Text = "Select";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.BtnSelectFile_Click);
            // 
            // txtFile
            // 
            this.txtFile.Location = new System.Drawing.Point(119, 44);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(328, 22);
            this.txtFile.TabIndex = 5;
            // 
            // txtSheetName
            // 
            this.txtSheetName.Location = new System.Drawing.Point(472, 44);
            this.txtSheetName.Name = "txtSheetName";
            this.txtSheetName.Size = new System.Drawing.Size(100, 22);
            this.txtSheetName.TabIndex = 6;
            this.txtSheetName.TextChanged += new System.EventHandler(this.TxtSheetName_TextChanged);
            // 
            // lblSheetName
            // 
            this.lblSheetName.AutoSize = true;
            this.lblSheetName.Location = new System.Drawing.Point(453, 47);
            this.lblSheetName.Name = "lblSheetName";
            this.lblSheetName.Size = new System.Drawing.Size(13, 17);
            this.lblSheetName.TabIndex = 7;
            this.lblSheetName.Text = "-";
            // 
            // btnSaveComment
            // 
            this.btnSaveComment.Location = new System.Drawing.Point(578, 76);
            this.btnSaveComment.Name = "btnSaveComment";
            this.btnSaveComment.Size = new System.Drawing.Size(75, 29);
            this.btnSaveComment.TabIndex = 8;
            this.btnSaveComment.Text = "Select";
            this.btnSaveComment.UseVisualStyleBackColor = true;
            this.btnSaveComment.Click += new System.EventHandler(this.BtnSaveComment_Click);
            // 
            // txtLinkSaveComment
            // 
            this.txtLinkSaveComment.Location = new System.Drawing.Point(119, 79);
            this.txtLinkSaveComment.Name = "txtLinkSaveComment";
            this.txtLinkSaveComment.Size = new System.Drawing.Size(453, 22);
            this.txtLinkSaveComment.TabIndex = 9;
            this.txtLinkSaveComment.TextChanged += new System.EventHandler(this.TxtLinkSaveComment_TextChanged);
            // 
            // lblLinkSaveComment
            // 
            this.lblLinkSaveComment.AutoSize = true;
            this.lblLinkSaveComment.Location = new System.Drawing.Point(12, 82);
            this.lblLinkSaveComment.Name = "lblLinkSaveComment";
            this.lblLinkSaveComment.Size = new System.Drawing.Size(101, 17);
            this.lblLinkSaveComment.TabIndex = 10;
            this.lblLinkSaveComment.Text = "Save comment";
            // 
            // btnGetComment
            // 
            this.btnGetComment.Location = new System.Drawing.Point(119, 107);
            this.btnGetComment.Name = "btnGetComment";
            this.btnGetComment.Size = new System.Drawing.Size(143, 29);
            this.btnGetComment.TabIndex = 11;
            this.btnGetComment.Text = "Get all comment";
            this.btnGetComment.UseVisualStyleBackColor = true;
            this.btnGetComment.Click += new System.EventHandler(this.BtnGetComment_Click);
            // 
            // btnReadCommentData
            // 
            this.btnReadCommentData.Location = new System.Drawing.Point(268, 107);
            this.btnReadCommentData.Name = "btnReadCommentData";
            this.btnReadCommentData.Size = new System.Drawing.Size(143, 29);
            this.btnReadCommentData.TabIndex = 12;
            this.btnReadCommentData.Text = "Read comment data";
            this.btnReadCommentData.UseVisualStyleBackColor = true;
            this.btnReadCommentData.Click += new System.EventHandler(this.BtnReadCommentData_Click);
            // 
            // btnTranslate
            // 
            this.btnTranslate.Location = new System.Drawing.Point(417, 107);
            this.btnTranslate.Name = "btnTranslate";
            this.btnTranslate.Size = new System.Drawing.Size(143, 29);
            this.btnTranslate.TabIndex = 13;
            this.btnTranslate.Text = "Translate";
            this.btnTranslate.UseVisualStyleBackColor = true;
            this.btnTranslate.Click += new System.EventHandler(this.BtnTranslate_Click);
            // 
            // grdComment
            // 
            this.grdComment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdComment.Location = new System.Drawing.Point(15, 232);
            this.grdComment.Name = "grdComment";
            this.grdComment.RowTemplate.Height = 24;
            this.grdComment.Size = new System.Drawing.Size(638, 214);
            this.grdComment.TabIndex = 14;
            // 
            // txtFileOutputEncoding
            // 
            this.txtFileOutputEncoding.Location = new System.Drawing.Point(119, 142);
            this.txtFileOutputEncoding.Name = "txtFileOutputEncoding";
            this.txtFileOutputEncoding.Size = new System.Drawing.Size(453, 22);
            this.txtFileOutputEncoding.TabIndex = 15;
            this.txtFileOutputEncoding.Text = "UTF-8";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Encoding";
            // 
            // frmTranslater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 458);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFileOutputEncoding);
            this.Controls.Add(this.grdComment);
            this.Controls.Add(this.btnTranslate);
            this.Controls.Add(this.btnReadCommentData);
            this.Controls.Add(this.btnGetComment);
            this.Controls.Add(this.lblLinkSaveComment);
            this.Controls.Add(this.txtLinkSaveComment);
            this.Controls.Add(this.btnSaveComment);
            this.Controls.Add(this.lblSheetName);
            this.Controls.Add(this.txtSheetName);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.btnSelectFile);
            this.Controls.Add(this.btnSelectFolder);
            this.Controls.Add(this.txtFolder);
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.lblSelectFolder);
            this.Name = "frmTranslater";
            this.Text = "Translater";
            ((System.ComponentModel.ISupportInitialize)(this.grdComment)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog dlgSelectFolder;
        private System.Windows.Forms.OpenFileDialog dlgSelectFile;
        private System.Windows.Forms.Label lblSelectFolder;
        private System.Windows.Forms.Label lblFile;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnSelectFolder;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.TextBox txtSheetName;
        private System.Windows.Forms.Label lblSheetName;
        private System.Windows.Forms.Button btnSaveComment;
        private System.Windows.Forms.TextBox txtLinkSaveComment;
        private System.Windows.Forms.Label lblLinkSaveComment;
        private System.Windows.Forms.FolderBrowserDialog dlgSelectFolderSaveComment;
        private System.Windows.Forms.Button btnGetComment;
        private System.Windows.Forms.Button btnReadCommentData;
        private System.Windows.Forms.Button btnTranslate;
        private System.Windows.Forms.DataGridView grdComment;
        private System.Windows.Forms.TextBox txtFileOutputEncoding;
        private System.Windows.Forms.Label label1;
    }
}

