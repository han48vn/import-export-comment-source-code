﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CommentTranslater
{

    /// <summary>
    /// Form translater
    /// </summary>
    public partial class FrmTranslater : Form
    {

        // Patch của folder source
        static string FOLDER_PATH = @"D:\Working\HAN48";
        // Định nghĩa các loại file source
        static string[] FILE_TYPE = new string[] { ".cs", ".js", ".java", ".php", ".c", ".cpp" };
        // File name của file comment đã được dịch
        static string FILE_NAME = @"D:\comment.xlsx";
        // Sheet chưa phần dịch trong file comment đã được dịch
        static string SHEET_NAME = "Sheet1";
        // Path của folder lưu file comment đã get được
        static string FOLDER_SAVE = @"D:\";

        // List tất cả các file source code (map với FOLDER_PATH và FILE_TYPE).
        static List<FileInfo> LIST_FILE = null;

        static Dictionary<string, string> COMMENT_DICTIONARY = null;

        // Danh sách các file bỏ qua ko cần get comment hoặc translate
        static string IGNORE_FILE = null;
        // Danh sách các folder bỏ qua ko cần get comment hoặc translate
        static string IGNORE_FOLDER = null;
        // Định nghĩa các ký tự định nghĩa comment
        static List<string> DEFINE_COMMENT = null;
        static List<string> DEFINE_COMMENT_START_WITH = null;

        /// <summary>
        /// Khởi tạo form
        /// </summary>
        public FrmTranslater()
        {
            // Khởi tạo màn hình
            InitializeComponent();
            // Khởi tạo các giá trị mặc định
            InitForm();
        }

        /// <summary>
        /// Khởi tạo các giá trị mặc định
        /// </summary>
        private void InitForm()
        {
            // Khởi tạo text box folder source
            txtFolder.Text = FOLDER_PATH;
            // Khởi tạo text box file comment đã translate
            txtFile.Text = FILE_NAME;
            // Khởi tạo text box sheet name
            txtSheetName.Text = SHEET_NAME;
            // Khởi tạo text box folder lưu comment get được
            txtLinkSaveComment.Text = FOLDER_SAVE;
            // Khởi tạo giá trị mặc định của IGNORE_FILE trong trường hợp chưa được define.
            if (IGNORE_FILE == null)
            {
                IGNORE_FILE = "(Designer.cs)|(AssemblyInfo.cs)|(TemporaryGeneratedFile)|(b0042)|(ganttchart)|(jquery)|(other)|(qunit)|(event)|(GlobalSuppressions)".ToLower();
            }
            // Khởi tạo giá trị mặc định của IGNORE_FOLDER trong trường hợp chưa được define.
            if (IGNORE_FOLDER == null)
            {
                IGNORE_FOLDER = "(Demo)|(bin)|(obj)|(.vs)|(Properties)|(.settings)".ToLower();
            }
            // Khởi tạo giá trị mặc định của DEFINE_COMMENT trong trường hợp chưa được define.
            if (DEFINE_COMMENT == null || DEFINE_COMMENT.Count == 0)
            {
                DEFINE_COMMENT = new List<string>
                {
                    "//",
                    "/*",
                    "*/"
                };
            }
            // Khởi tạo giá trị mặc định của DEFINE_COMMENT_START_WITH trong trường hợp chưa được define.
            if (DEFINE_COMMENT_START_WITH == null || DEFINE_COMMENT_START_WITH.Count == 0)
            {
                DEFINE_COMMENT_START_WITH = new List<string>
                {
                    "*"
                };
            }
        }

        /// <summary>
        /// Gét toàn bộ file
        /// </summary>
        private void getFile(string folderName)
        {
            // Khởi tạo giá trị mặc định của LIST_FILE
            LIST_FILE = new List<FileInfo>();
            // Get tất cả các file trong folder
            LIST_FILE = getFileFromFolder(folderName).Distinct().ToList();
            return;
        }

        /// <summary>
        /// Đệ quy get toàn bộ file trong folder
        /// </summary>
        /// <param name="folderPath"></param>
        /// <returns></returns>
        private List<FileInfo> getFileFromFolder(string folderPath)
        {
            DirectoryInfo folder = new DirectoryInfo(folderPath);
            if (folder.Exists)
            {
                ;
            }
            else
            {
                return new List<FileInfo>();
            }
            // Trường hợp IGNORE_FILE hoặc IGNORE_FOLDER là NULL thì thực hiện khởi tọa giá trị mặc định
            if (IGNORE_FILE == null || IGNORE_FOLDER == null)
            {
                // Khởi tạo giá trị mặc định
                InitForm();
            }
            // Kiểm tra folder có chưa trong danh sách IGNORE_FOLDER
            // Nếu có thì return list rỗng
            if (System.Text.RegularExpressions.Regex.IsMatch(folderPath.ToLower(), IGNORE_FOLDER))
            {
                return new List<FileInfo>();
            }
            // Get thông tin của folder
            DirectoryInfo directory = new DirectoryInfo(folderPath);
            // Khởi tạo list file dùng để return
            List<FileInfo> listFile = new List<FileInfo>();
            // Duyệt toàn bộ FILE_TYPE
            foreach (string type in FILE_TYPE)
            {
                // Trường hợp type không bắt đầu bằng * thì add thêm * vào đầu
                string realType = type;
                if (!realType.StartsWith("*"))
                {
                    realType = "*" + realType;
                }
                // Get toàn bộ các file trong folder gốc map với file type.
                FileInfo[] infoes = directory.GetFiles(realType);
                // Duyệt toàn bộ file trong folder
                foreach (FileInfo file in infoes)
                {
                    // Trường hợp file không có trong danh sách IGNORE_FILE
                    // Thì add vào list dùng để return
                    if (!System.Text.RegularExpressions.Regex.IsMatch(file.Name.ToLower(), IGNORE_FILE))
                    {
                        listFile.Add(file);
                    }
                }
            }
            string[] allFolder = Directory.GetDirectories(folderPath);
            // Duyệt tiếp các folder có trong folder gốc
            foreach (string FolderName in allFolder)
            {
                // Gọi đệ quy và add list file vào list dùng để return.
                listFile.AddRange(getFileFromFolder(FolderName));
            }
            // Trả về list các file có trong folder.
            return listFile;
        }

        /// <summary>
        /// Action khi click button select folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSelectFolder_Click(object sender, EventArgs e)
        {
            // Hiển thị dialog chọn folder
            DialogResult result = dlgSelectFolder.ShowDialog();
            // Trường hợp kết quả là OK (đã chọn folder)
            if (result == DialogResult.OK)
            {
                // Get folder path nhận từ dialog và gán vào FOLDER_PATH
                FOLDER_PATH = dlgSelectFolder.SelectedPath;
                // Trường hợp folder path không đổi thì xử lý get lại file
                if (txtFolder.Text == FOLDER_PATH)
                {
                    // Khởi tạo list file và get lại file trong folder
                    LIST_FILE = new List<FileInfo>();
                    getFile(FOLDER_PATH);
                }
                // Trường hợp folder path có thay đổi thì xử lý get file sẽ thực hiện trong event TextChanged
                // Gán giá trị FOLDER_PATH cho text box Folder
                txtFolder.Text = FOLDER_PATH;
            }
        }

        /// <summary>
        /// Action khi click button set link save comment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSaveComment_Click(object sender, EventArgs e)
        {
            // Hiển thị dialog chọn folder save file
            DialogResult result = dlgSelectFolderSaveComment.ShowDialog();
            // Trường hợp kết quả là OK (đã chọn folder)
            if (result == DialogResult.OK)
            {
                // Get folder path nhận từ dialog và gán vào FOLDER_SAVE
                FOLDER_SAVE = dlgSelectFolderSaveComment.SelectedPath;
                // Gán giá trị FOLDER_SAVE cho text box link save comment
                txtLinkSaveComment.Text = FOLDER_SAVE;
            }
        }

        /// <summary>
        /// Action khi click button get comment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnGetComment_Click(object sender, EventArgs e)
        {
            // Trường hợp FOLDER_SAVE null hoặc blank, LIST_FILE null hoặc rỗng thì return.
            if (FOLDER_SAVE == null || FOLDER_SAVE == "" || LIST_FILE == null || LIST_FILE.Count == 0)
            {
                // Show message warning: Please select folder và save comment!
                MessageBox.Show("Please select folder và save comment!", "Warning...");
                return;
            }
            // Get toàn bộ comment source code
            List<string> allComment = GetAllComment(LIST_FILE);
            // Trường hợp file comment đã tồn tại thì xóa file cũ.
            if (File.Exists(FOLDER_SAVE + @"\ALL_COMMENT.CSV"))
            {
                File.Delete(FOLDER_SAVE + @"\ALL_COMMENT.CSV");
            }
            // Create file csv comment source code
            CreateCSVFile(allComment);
        }

        /// <summary>
        /// Action khi click button select file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSelectFile_Click(object sender, EventArgs e)
        {
            // Hiển thị dialog và chỉ get các file Excel.
            dlgSelectFile.Filter = "Excel Files (.xls, .xlsx)|*.xlsx;*.xls";
            DialogResult result = dlgSelectFile.ShowDialog();
            // Trường hợp kết quả là OK (đã chọn file)
            if (result == DialogResult.OK)
            {
                // Get file name nhận từ dialog và gán vào FILE_NAME
                FILE_NAME = dlgSelectFile.FileName;
                // Gán giá trị FILE_NAME cho text box file
                txtFile.Text = FILE_NAME;
            }
        }

        /// <summary>
        /// Read comment data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnReadCommentData_Click(object sender, EventArgs e)
        {
            // Read comment translated
            ReadCommentTranslated();
        }

        /// <summary>
        /// Action khi click button translate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnTranslate_Click(object sender, EventArgs e)
        {
            ReadCommentTranslated();
            // Trường hợp FOLDER_PATH null hoặc blank, COMMENT_DICTIONARY null hoặc rỗng thì return.
            if (FOLDER_PATH == null || FOLDER_PATH == "" || COMMENT_DICTIONARY == null || COMMENT_DICTIONARY.Count == 0)
            {
                // Show message warning: Please select folder và read comment data!
                MessageBox.Show("Please select folder và read comment data!", "Warning...");
                return;
            }
            // Translate all comment and get comment untranslate.
            List<string> untranslated = TranslateAllComment(LIST_FILE);
            if (untranslated != null && untranslated.Count > 0)
            {
                CreateCSVFile(untranslated);
            }
        }

        /// <summary>
        /// Action khi text box folder thay đổi giá trị
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtFolder_TextChanged(object sender, EventArgs e)
        {
            // Set giá trị FOLDER_PATH bằng với giá trị được điền trong text box folder.
            FOLDER_PATH = txtFolder.Text;
            // Get lại file
            LIST_FILE = new List<FileInfo>();
            getFile(FOLDER_PATH);
        }

        /// <summary>
        /// Action khi text box link save comment thay đổi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtLinkSaveComment_TextChanged(object sender, EventArgs e)
        {
            // Gán giá trị FOLDER_SAVE bằng với giá trị được điền trong text box link save comment.
            FOLDER_SAVE = txtLinkSaveComment.Text;
        }

        /// <summary>
        /// Action khi text box sheet name thay đổi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtSheetName_TextChanged(object sender, EventArgs e)
        {
            // Gán giá trị SHEET_NAME bằng với giá trị được điền trong text box sheet name.
            SHEET_NAME = txtSheetName.Text;
        }

        /// <summary>
        /// Create csv file.
        /// </summary>
        /// <param name="allComment"></param>
        private void CreateCSVFile(List<string> allComment)
        {
            // Tạo stream ghi file, cho phép ghi file với ENCODE là UTF-8
            using (var w = new StreamWriter(FOLDER_SAVE + @"\ALL_COMMENT.CSV", false, Encoding.UTF8))
            {
                // Get format của header CSV
                string write = GetCVSLine("Source", "Translated");
                // Ghi nội dung header vào file.
                w.WriteLine(write);
                w.Flush();
                // Duyệt toàn bộ nội dung comment và ghi vào file comment.
                foreach (string comment in allComment)
                {
                    // Get format của CSV
                    write = GetCVSLine(comment, "");
                    // Ghi nội dung comment vào file.
                    w.WriteLine(write);
                    w.Flush();
                }
                // Show message info: Exported all comment source code!
                MessageBox.Show("Exported all comment source code!", "Info");
            }
        }

        /// <summary>
        /// Đọc file excel
        /// Sử dụng phương pháp coi file excel như là 1 database
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> ReadExcel(string fileName, string sheetName)
        {
            // Khởi tạo giá trị dùng để return
            Dictionary<string, string> result = new Dictionary<string, string>();
            // Khởi tạo OleDbCommand
            // Khởi tạo connection string, file name là FILE_NAME
            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            // Khởi tạo câu lệnh query, tên sheet là SHEET_NAME
            string query = "SELECT * FROM [" + sheetName + "$]";
            // Connect đến file excel
            using (OleDbConnection conn = new OleDbConnection(connString))
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                // Đọc nội dung trong sheet của excel
                OleDbCommand cmd = new OleDbCommand(query, conn);
                using (OleDbDataReader dr = cmd.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        // Duyệt toàn bộ nội dung trong excel
                        while (dr.Read())
                        {
                            // Trường hợp comment chưa tồn tại thì add comment vào danh sách comment
                            if (!result.ContainsKey(dr[0] + ""))
                            {
                                result.Add(dr[0] + "", dr[1] + "");
                            }
                        }
                    }
                }
            }
            // Trả về danh sách comment
            return result;
        }

        /// <summary>
        /// Get format của 1 line trong CSV
        /// Paramter truyền vào có thể là 1 hoặc nhiều paramter loại string.
        /// </summary>
        /// <param name="messages"></param>
        /// <returns></returns>
        private string GetCVSLine(params string[] messages)
        {
            // Khởi tạo giá trị trả về
            string result = "";
            // Duyệt toàn bộ paramter
            for (int i = 0; i < messages.Length; i++)
            {
                // Format chung của item trong CSV là "{giá trị}"
                // Ghép chuỗi với giá trị trả vè
                result += string.Format("\"{0}\"", messages[i]);
                // Trường hợp không phỉa là giá trị cuối cùng thì add thêm "," vào cuối giá trị trả về
                if (i != messages.Length - 1)
                {
                    result += ",";
                }
            }
            // Trả về chuỗi đã được format.
            return result;
        }

        /// <summary>
        /// Get all comment in file
        /// </summary>
        /// <returns></returns>
        private List<string> GetAllComment(List<FileInfo> listFile)
        {
            if (listFile == null)
            {
                throw new ArgumentNullException(nameof(listFile));
            }
            // Tạo stream ghi file, cho phép ghi file với ENCODE là UTF-8
            Encoding encoding = Encoding.GetEncoding(txtFileOutputEncoding.Text);
            // Khởi tạo giá trị dùng để return
            List<string> result = new List<string>();
            // Trường hợp không có file thì không xử lý gì
            if (listFile.Count > 0)
            {
                // Duyệt toàn bộ file trong LIST_FILE
                foreach (FileInfo file in listFile)
                {
                    // Đọc nội dung trong file.
                    string[] lines = File.ReadAllLines(file.FullName, encoding);
                    // Duyệt toàn bộ nội dung trong file
                    foreach (string line in lines)
                    {
                        string realData = line.Trim();
                        // Trường hợp nội dung có chưa comment thì xem xét add vào list comment
                        foreach (string comment in DEFINE_COMMENT)
                        {
                            if (realData.Contains(comment))
                            {
                                // Trường hợp comment chưa tồn tại thì add vào list comment
                                if (!result.Contains(realData))
                                {
                                    result.Add(realData);
                                }
                            }
                        }
                        foreach (string comment in DEFINE_COMMENT_START_WITH)
                        {
                            if (realData.StartsWith(comment))
                            {
                                // Trường hợp comment chưa tồn tại thì add vào list comment
                                if (!result.Contains(realData))
                                {
                                    result.Add(realData);
                                }
                            }
                        }
                    }
                }
            }
            // Trả về list comment
            return result;
        }

        /// <summary>
        /// Read comment translated
        /// </summary>
        private void ReadCommentTranslated()
        {
            // Read file excel data
            COMMENT_DICTIONARY = ReadExcel(FILE_NAME, SHEET_NAME);
            // Fill data to grid view
            grdComment.DataSource = COMMENT_DICTIONARY.ToArray();
            grdComment.Columns[0].HeaderText = "Source";
            grdComment.Columns[1].HeaderText = "Translated";
        }

        private List<string> TranslateAllComment(List<FileInfo> listFile)
        {
            if (listFile == null)
            {
                throw new ArgumentNullException(nameof(listFile));
            }
            // Tạo stream ghi file, cho phép ghi file với ENCODE là UTF-8
            Encoding encoding = null;
            if (txtFileOutputEncoding.Text.ToLower().Contains("utf-8"))
            {
                encoding = new UTF8Encoding(txtFileOutputEncoding.Text.ToLower().Contains("bom"));
            }
            else
            {
                Encoding.GetEncoding(txtFileOutputEncoding.Text);
            }
            // Khởi tạo giá trị dùng để return
            List<string> result = new List<string>();
            // Trường hợp không có file thì không xử lý gì
            if (listFile != null && listFile.Count > 0)
            {
                // Duyệt toàn bộ file trong LIST_FILE
                foreach (FileInfo file in listFile)
                {
                    // Đọc nội dung trong file.
                    string[] lines = File.ReadAllLines(file.FullName, encoding);
                    List<string> newLines = new List<string>(); 
                    // Duyệt toàn bộ nội dung trong file
                    foreach (string line in lines)
                    {
                        bool replaced = false;
                        string realData = line.Trim();
                        foreach (string comment in DEFINE_COMMENT)
                        {
                            if (realData.Contains(comment))
                            {
                                // Trường hợp comment chưa tồn tại thì add vào list comment
                                if (!COMMENT_DICTIONARY.ContainsKey(realData))
                                {
                                    newLines.Add(line);
                                    result.Add(line);
                                    replaced = true;
                                    break;
                                }
                                else
                                {
                                    newLines.Add(line.Replace(realData, COMMENT_DICTIONARY[realData]));
                                    replaced = true;
                                    break;
                                }
                            }
                        }
                        if (replaced)
                        {
                            continue;
                        }
                        else
                        {
                            ;
                        }
                        foreach (string comment in DEFINE_COMMENT_START_WITH)
                        {
                            if (realData.StartsWith(comment))
                            {
                                // Trường hợp comment chưa tồn tại thì add vào list comment
                                if (!COMMENT_DICTIONARY.ContainsKey(realData))
                                {
                                    newLines.Add(line);
                                    result.Add(line);
                                    replaced = true;
                                    break;
                                }
                                else
                                {
                                    newLines.Add(line.Replace(realData, COMMENT_DICTIONARY[realData]));
                                    replaced = true;
                                    break;
                                }
                            }
                        }
                        // Trường hợp nội dung có chưa comment thì xem xét add vào list comment
                        if (replaced)
                        {
                            continue;
                        }
                        else
                        {
                            newLines.Add(line);
                            continue;
                        }
                    }
                    using (var w = new StreamWriter(file.FullName, false, encoding))
                    {
                        // Duyệt toàn bộ nội dung comment và ghi vào file comment.
                        foreach (string line in newLines)
                        {
                            // Ghi nội dung comment vào file.
                            w.WriteLine(line);
                            w.Flush();
                        }
                    }
                }
            }
            // Show message info: Exported all comment source code!
            MessageBox.Show("Translat completed!", "Info");
            // Trả về list comment
            return result;
        }


    }

}
