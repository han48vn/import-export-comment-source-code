using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SourceBeforeTranslate.Models;

namespace SourceBeforeTranslate.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            ASP.NET id モデルをカスタマイズし、必要に応じて既定値をオーバーライドします。
            たとえば、ASP.NET id テーブル名などの名前を変更できます。
            ベースの呼び出し後にカスタマイズを追加します。OnModelCreating (ビルダー);
        }
    }
}
