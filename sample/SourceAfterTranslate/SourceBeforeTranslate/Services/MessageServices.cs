using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SourceBeforeTranslate.Services
{
    このクラスは、電子メールと SMS を送信するためにアプリケーションによって使用されます
    ASP.NET id で2要素認証を有効にするとします。
    詳細については、このリンクを参照してください https://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        public Task SendEmailAsync(string email, string subject, string message)
        {
            電子メールを送るためにあなたの電子メールサービスをここに差し込みなさい。
            return Task.FromResult(0);
        }

        public Task SendSmsAsync(string number, string message)
        {
            SMS サービスをプラグインして、テキストメッセージを送信します。
            return Task.FromResult(0);
        }
    }
}
